package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

var verbose bool = false
var quiet bool = false
var csv bool = false
var ipType int = 0
var ipString string

func main() {
	if len(os.Args) < 2 {
		os.Args = append(os.Args, "-h")
	}

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "-h" || arg == "--help" {
			fmt.Print("-h | --help\t\t\t\t\tPrint this help message.\n")
			fmt.Print("-q | --quiet\t\t\t\t\tSupress output of individual addresses.\n")
			fmt.Print("-v | --verbose\t\t\t\t\tPrint Network ID, Broadcast, Netmask, Hostmask.\n")
			fmt.Print("--csv\t\t\t\t\tPrint addresses as csv.\n")
			fmt.Print("A.B.C.D/XY\t\t\t\t\tProcess IPv4 Address.\n")
			fmt.Print("12AB:34CD:56EF:78AB:90CD:12EF:34AB:56CD/XY\tProcess IPv6 Address.\n")
		}

		if arg == "-v" || arg == "--verbose" {
			verbose = true
		}

		if arg == "-q" || arg == "--quiet" {
			quiet = true
		}

		if arg == "--csv" {
			csv = true
		}

		// Match IPv4 CIDR
		regex := regexp.MustCompile(`(\d+\.){3}\d+/\d+`)
		if regex.MatchString(arg) {
			ipType = 4
			ipString = arg
		}

		// Match IPv6 CIDR
		regex = regexp.MustCompile(`([0-9a-fA-F]{4}:){7}[0-9a-fA-F]{4}/\d+`)
		if regex.MatchString(arg) {
			ipType = 6
			ipString = arg
		}
	}

	if ipType == 0 {
		er(fmt.Errorf("No valid CIDR IP address given"))
	}

	var ip uint32
	var netmask uint32
	var hostmask uint32
	var networkid uint32
	var broadcast uint32

	if ipType == 4 {
		cidrArray := strings.Split(ipString, "/")

		ipArray := strings.Split(cidrArray[0], ".")

		networkBits, err := strconv.Atoi(cidrArray[1])
		er(err)

		if networkBits < 0 || networkBits > 32 {
			er(fmt.Errorf("invalid network bit (%d) in %s", networkBits, ipString))
		}

		hostBits := 32 - networkBits

		// Convert ip octets (strings) to one 32bit int
		for i := 0; i < len(ipArray); i++ {
			octet, err := strconv.Atoi(ipArray[i])
			er(err)

			if octet < 0 || octet > 255 {
				er(fmt.Errorf("invalid octet (%d) in %s", octet, ipString))
			}

			ip = ip << 8
			ip = ip + uint32(octet)
		}

		// Convert hostbits to hostmask (uint32)
		for i := 0; i < hostBits; i++ {
			hostmask = hostmask << 1
			hostmask++
		}

		// Derive netmask from hostmask
		netmask = hostmask ^ 0xFFFFFFFF

		// Derive network id broadcast
		networkid = ip & netmask
		broadcast = ip | hostmask

		if verbose {
			fmt.Printf("Network ID:   %08b.%08b.%08b.%08b\t%s\n", byte(networkid>>24), byte(networkid>>16), byte(networkid>>8), byte(networkid), ipv4Int2String(networkid))
			fmt.Printf("Broadcast:    %08b.%08b.%08b.%08b\t%s\n", byte(broadcast>>24), byte(broadcast>>16), byte(broadcast>>8), byte(broadcast), ipv4Int2String(broadcast))
			fmt.Printf("Network Mask: %08b.%08b.%08b.%08b\t%s\n", byte(netmask>>24), byte(netmask>>16), byte(netmask>>8), byte(netmask), ipv4Int2String(netmask))
			fmt.Printf("Host Mask:    %08b.%08b.%08b.%08b\t%s\n", byte(hostmask>>24), byte(hostmask>>16), byte(hostmask>>8), byte(hostmask), ipv4Int2String(hostmask))
		}

		if !quiet {
			for i := networkid; i <= broadcast; i++ {
				if csv {
					fmt.Printf("%s", ipv4Int2String(i))
					if i != broadcast {
						fmt.Print(", ")
					} else {
						fmt.Print("\n")
					}
				} else {
					fmt.Printf("%s\n", ipv4Int2String(i))
				}
			}
		}

	}

	if ipType == 6 {
		var ipv6 [8]uint16
		var netmaskv6 [8]uint16
		var hostmaskv6 [8]uint16
		var networkidv6 [8]uint16
		var broadcastv6 [8]uint16

		cidrArray6 := strings.Split(ipString, "/")

		ipv6Array := strings.Split(cidrArray6[0], ":")

		for i := 0; i < len(ipv6Array); i++ {

			tmp, err := strconv.ParseUint(ipv6Array[i], 16, 16)
			er(err)

			ipv6[i] = uint16(tmp)
		}

		networkBits6, err := strconv.Atoi(cidrArray6[1])
		er(err)

		if networkBits6 < 0 || networkBits6 > 128 {
			er(fmt.Errorf("invalid network bit (%d) in %s", networkBits6, ipString))
		}

		//	hostBits6 := 128 - networkBits6

		for i := networkBits6; i >= 0; i-- {
			index := 0

			if i > 16 && i <= 32 {
				index = 1
			}
			if i > 32 && i <= 48 {
				index = 2
			}
			if i > 48 && i <= 64 {
				index = 3
			}
			if i > 64 && i <= 80 {
				index = 4
			}
			if i > 80 && i <= 96 {
				index = 5
			}
			if i > 96 && i <= 112 {
				index = 6
			}
			if i > 112 && i <= 128 {
				index = 7
			}

			for j := 0; j < index; j++ {
				netmaskv6[j] = 0xFFFF
			}

			for k := i % 16; k >= 0; k-- {
				netmaskv6[index] = netmaskv6[index] << 1
				netmaskv6[index]++
			}
		}

		// Derive netmask, network id, and broadcast

		for i := 0; i < 8; i++ {
			netmaskv6[i] = hostmaskv6[i] ^ 0xFFFF
			networkidv6[i] = ipv6[i] & netmaskv6[i]
			broadcastv6[i] = ipv6[i] | hostmaskv6[i]
		}

		fmt.Println(ipString)
		fmt.Println(ipv6, ipv6Int2String(ipv6))
		fmt.Println(networkidv6, ipv6Int2String(networkidv6))
		fmt.Println(broadcastv6, ipv6Int2String(broadcastv6))
		fmt.Println(netmaskv6, ipv6Int2String(netmaskv6))
		fmt.Println(hostmaskv6, ipv6Int2String(hostmaskv6))

		if verbose {
			fmt.Printf("Network ID:   %016b:%016b:%016b:%016b:%016b:%016b:%016b:%016b\t%s\n", uint16(networkidv6[0]>>48), uint16(networkidv6[0]>>32), uint16(networkidv6[0]>>16), uint16(networkidv6[0]), uint16(networkidv6[1]>>48), uint16(networkidv6[1]>>32), uint16(networkidv6[1]>>16), uint16(networkidv6[1]), ipv6Int2String(networkidv6))
			fmt.Printf("Broadcast:    %016b:%016b:%016b:%016b:%016b:%016b:%016b:%016b\t%s\n", uint16(broadcastv6[0]>>48), uint16(broadcastv6[0]>>32), uint16(broadcastv6[0]>>16), uint16(broadcastv6[0]), uint16(broadcastv6[1]>>48), uint16(broadcastv6[1]>>32), uint16(broadcastv6[1]>>16), uint16(broadcastv6[1]), ipv6Int2String(broadcastv6))
			fmt.Printf("Network Mask: %016b:%016b:%016b:%016b:%016b:%016b:%016b:%016b\t%s\n", uint16(netmaskv6[0]>>48), uint16(netmaskv6[0]>>32), uint16(netmaskv6[0]>>16), uint16(netmaskv6[0]), uint16(netmaskv6[1]>>48), uint16(netmaskv6[1]>>32), uint16(netmaskv6[1]>>16), uint16(netmaskv6[1]), ipv6Int2String(netmaskv6))
			fmt.Printf("Host Mask:    %016b:%016b:%016b:%016b:%016b:%016b:%016b:%016b\t%s\n", uint16(hostmaskv6[0]>>48), uint16(hostmaskv6[0]>>32), uint16(hostmaskv6[0]>>16), uint16(hostmaskv6[0]), uint16(hostmaskv6[1]>>48), uint16(hostmaskv6[1]>>32), uint16(hostmaskv6[1]>>16), uint16(hostmaskv6[1]), ipv6Int2String(hostmaskv6))
		}

		if !quiet {
			for i := networkid; i <= broadcast; i++ {
				if csv {
					fmt.Printf("%s", ipv4Int2String(i))
					if i != broadcast {
						fmt.Print(", ")
					} else {
						fmt.Print("\n")
					}
				} else {
					fmt.Printf("%s\n", ipv4Int2String(i))
				}
			}
		}
	}

}

func ipv4Int2String(ipInt uint32) string {
	var output string

	output = strconv.Itoa(int(ipInt << 0 >> 24))
	output = output + "."
	output = output + strconv.Itoa(int(ipInt<<8>>24))
	output = output + "."
	output = output + strconv.Itoa(int(ipInt<<16>>24))
	output = output + "."
	output = output + strconv.Itoa(int(ipInt<<24>>24))

	return output
}

func ipv6Int2String(ipInt [8]uint16) string {
	var output string
	output = fmt.Sprintf("%04X:%04X:%04X:%04X:%04X:%04X:%04X:%04X", ipInt[0], ipInt[1], ipInt[2], ipInt[3], ipInt[4], ipInt[5], ipInt[6], ipInt[7])
	return output
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
